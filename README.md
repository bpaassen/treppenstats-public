# Treppenstats-Datensatz

Copyright (C) 2022-2024 - Benjamin Paaßen

Dieser Datensatz beschreibt auf maschinenlesbare Weise den Verlauf des Filmquiz ["Wendeltreppe ins Nichts"](https://wendeltreppe.org). Das Quiz funktioniert nach folgenden Regeln: In jeder Episode treten zwei Kontrahent\*innen gegeinander an. Das Ziel ist es, möglichst rasch Filme auf Basis von Rezensionen zu erraten. Jede Episode hat fünf Runden. In jeder Runde gilt es, einen Film zu erraten. Dazu liest die\*der Moderatorin eine Rezension (oder einen Ausschnitt daraus) vor und gibt danach den Treppenkontrahent\*innen Zeit, Filmtitel zu raten (mit Formulierungen wie "Ist es *Fight Club*?" oder "Aber es ist nicht *Fight Club*, oder?"). Sobald das Raten zum Erliegen kommt, liest die\*der Moderator\*in weitere Rezensionen vor oder gibt andere Informationen, bis ein\*e Kontrahent\*in den richtigen Film errät.

Der Datensitz gibt Auskunft darüber, welche Filme von wem geraten wurden, aus welcher Quelle die Rezensionen stammten, wie lang jede Runde dauerte und wer moderierte. Der Datensatz ist händisch durch mich erstellt für das Wissenschaftskommunikations- und Fan-Projekt [treppenstats](https://bsky.app/profile/treppenstats.bsky.social), bei dem statistische Analysen auf den Daten ausgeführt werden.

Die Daten sind hier als [JSON-Datei](https://www.json.org/json-de.html) unter `data.json` gespeichert. Der Aufbau ist wie folgt:

```js
[
  {
    "index": 13,
    "title": "Wendeltreppe nach St. Pf\u00e4ffle",
    "url": "https://wendeltreppe.letscast.fm/episode/013-wendeltreppe-nach-st-pfaeffle-mit-david",
    "host": "Jan",
    "players": [
      "David",
      "Christiane"
    ],
    "rounds": [
      {
        "start_time": "23:53",
        "end_time": "35:11",
        "reviews": [
          {
            "source": "leinwandreporter.com",
            "guesses": [
              {
                "player": "David",
                "movie": "The Nightingale",
                "url": "https://letterboxd.com/film/the-nightingale-2018/",
                "correct": true,
                "shared": true
              }
            ]
          }
        ]
      }
    ]
  }
]
```

Die äußerste Struktur ist eine Liste von Episoden. Jede **Episode** ist mit den folgenden Feldern beschrieben:

* `index` ist der Index der Episode laut [Episodenliste](https://wendeltreppe.letscast.fm/index). Der Index ist fortlaufend von 0, allerdings fehlen einige Episoden aus Datenschutzgründen oder weil in der Runde nicht geraten wurde (Episode 15).
* `title` ist der Name bzw. Titel der Episode.
* `url` ist der Link zur Episode.
* `host` ist der Name der\*des Moderator\*in. Typischerweise ist das Christiane oder Jan. Ausnahmen sind Episode 0 (Christiane und Jan moderieren abwechselnd mit jeweils der\*dem anderen als einziger\*m Spieler\*in), 7 (Christiane und Jan moderieren abwechselnd), 8 (Becci moderiert, während Christiane gegen Jan spielt) und 16 (anonym moderiert, während Christiane gegen Jan spielt).
* `players` ist die Liste der Kontrahent\*innen. In der Liste steht üblicherweise ein Gast an erster Stelle und Christiane oder Jan an zweiter Stelle. Ausnahmen sind die oben genannten Episoden.
* `rounds` ist eine Liste von Runden. Es sind immer fünf Runden, außer in Episode 0 (nur zwei Runden).

Jede **Runde** ist durch folgende Felder beschrieben:

* `start_time` ist die Startzeit der Runde in Minuten und Sekunden in der hörbaren Podcast-Episode. Die Startzeit bezieht sich auf den Moment, an dem die\*der Moderator\*in beginnt, die erste Rezension vorzulesen. Ab diesem Moment können sich die Kontrahent\*innen bereits Gedanken über die Lösung machen, daher wird ab hier die Rundenzeit gezählt.
* `end_time` ist die Endzeit der Runde in Minuten und Sekunden in der hörbaren Podcast-Episode. Die Endzeit bezieht sich auf den Moment, an dem die\*der Moderator\*in bestätigt, dass der richtige Film erraten wurde.
* `reviews` ist eine Liste der Rezensionen, die vorgelesen wurden.

Jede **Rezension** ist wiederum durch folgende Felder beschrieben:

* `source` ist die Quelle, aus der die Rezension stammt. Das ist üblicherweise eine Website, die aufrufbar ist. In Ausnahmen steht in Klammern auch noch ein\*e Autor\*in dahinter, wenn häufiger Rezensionen der entsprechenden Person vorgelesen werden (z.B. `letterboxd.com (Kamil)`). Eine weitere Ausnahme ist `voicemail (Heiko)` in Episode 11, als extra für das Quiz eingesprochene Rezensionen abgespielt wurden. Schließlich gibt es eine Rezension in Episode 16, deren Quelle unbekannt ist.
* `guesses` ist eine Liste der Rateversuche, die auf die Rezension folgten.

Jeder **Rateversuch** ist durch folgende Felder beschrieben:

* `player` ist die Person, die geraten hat. Der Name ist immer konsistent mit der `players`-Liste aus der Episodenbeschreibung.
* `movie` ist der Name des Films, der geraten wurde. Der Name ist konsistent mit dem Namen auf [letterboxd.com](https://letterboxd.com/). Achtung! Dieser Name kann ambig sein.
* `url` ist die URL des Films auf [letterboxd.com](https://letterboxd.com/). Diese URL ist einmalig für jeden Film und kann daher zur Identifikation des Films benutzt werden. Die einzige Ausnahme ist ein Rateversuch bei Episode 6, Zeit 14:05 namens "Der Meerjungfrauenfilm", den ich nicht identifizieren konnte.
* `correct`: Falls der Rateversuch korrekt war ist das Feld `correct` mit dem Wert `true` hinterlegt.
* `shared`: Falls beide Kontrahent\*innen den Film gemeinsam erraten haben ist nicht nur das Feld `correct` sondern auch das Feld `shared` mit dem Wert `true` hinterlegt.

## Beschränkungen des Datensatzes

Der Datensatz hat mehrere Limitationen, die in Zukunft vielleicht korrigiert werden könnten.

* Der hier veröffentlichte Datensatz enthält nicht alle Episoden. Episode 15 ist nicht enthalten, weil darin nicht geraten wurde. Andere Episoden fehlen, weil ich das Einverständnis der Mitspieler\*innen für die Veröffentlichung (noch) nicht erhalten habe. Sollten weitere Einverständniserklärungen eintreffen werden die entsprechenden Episoden nachgepflegt.
* Weil alle Informationen händisch eingegeben sind kann es zu Daten-Eingabefehlern gekommen sein, z.B. copy&paste-Fehler, falsch zugeordnete Filme oder ungenaue Zeitstempel. Sollte dem so sein können mir Korrekturen gern über [treppenstats](https://bsky.app/profile/treppenstats.bsky.social) mitgeteilt werden.
* Ich habe nicht immer alle Rateversuche abgeschrieben. Mein Kriterium war, dass die Rateversuche den Formulierungen "Es ist ...", "Ist es ...?" oder "Es ist nicht ..." folgen mussten und/oder explizit von der Moderation kommentiert sein mussten. Das Kriterium habe ich vielleicht nicht 100% konsequent angewandt.
* Nur die Runden enthalten Zeitstempel. Es gibt bislang keine Zeitstempel für Rezensionen oder Rateversuche, weil mir das zu viel Arbeit erschien. Wer das gern nachpflegen möchte ist herzlich eingeladen.
* Die Quellen der Rezensionen enthalten bisher nur Websites und sehr selten die tatsächlichen Autor\*innen. Da die meisten Autor\*innen nur einmal genannt werden erschein mir die zusätzliche Arbeit nicht gerechtfertigt.

## Statistischer Analysecode

Wenn ihr die statistischen Analysen für [treppenstats](https://bsky.app/profile/treppenstats.bsky.social) genauer nachvollziehen wollt, könnt ihr euch das [Jupyter notebook](https://jupyter.org/) in der Datei `auswertung.ipynb` anschauen. Dafür braucht ihr eine [Python](https://www.python.org/)-Installation (Version 3) mit Jupyter und die Python-Pakete [numpy](https://numpy.org/), [matplotlib](https://matplotlib.org/) und [scipy](https://scipy.org/).

Achtung! Die Ergebnisse werden von denen in [treppenstats](https://bsky.app/profile/treppenstats.bsky.social) etwas abweichen, weil der Datensatz hier nicht alle Episoden enthält (siehe 'Beschränkungen').

## Lizenz

Der Datensatz ist hier unter [Creative Commons Non-Commercial Share Alike Version 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/) veröffentlicht (siehe `LICENSE.md` für den vollen Lizenz-Text. Das heißt: ihr dürft den Datensatz gebrauchen, wie ihr möchtet und auch eigene Kopien weitergeben, sofern das unter der gleichen Lizenz geschieht (Share Alike), auf die Quelle (also diese Seite hier) verweist (Attribution) und keine kommerziellen Zwecke verfolgt (non-commercial). Die Lizenz erstreckt sich auch auf den Analysecode in der `auswertung.ipynb`-Datei.
